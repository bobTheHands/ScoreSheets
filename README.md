Languages: **English** ***[Deutsch](README.de.md)***

# Score sheets
Result lists for the following games:

* Rummy
* Sixty-Six
* popular game Sk... King (here Pirates)
* popular game Wi..rd (here Magician)
* dice game Y...zee (here Pasch)
* and other games where the points must be added or substracted

## Features
* supported languages: english, german
* up to 8 players
* unlimited predefined player names
* Rummy: add double score with one click
* automatic calculation
* dealer indicator
* **does not require any permissions**

## Screenshots
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" alt="Screenshot Home" width="200"/>
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" alt="Screenshot Sixty-Six" width="200"/>
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" alt="Screenshot Pirates" width="200"/>
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.png" alt="Screenshot Pasch" width="200"/>

## Remarks
I made this Android app for my own private use. Maybe someone else will find it useful.

**If you suspect a violation of trademark law, please open an issue.**