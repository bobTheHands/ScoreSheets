Languages: ***[English](README.md)*** **Deutsch**

# Spieleblock
Ergebnislisten für die folgenden Spiele:

* Rommé
* Sechsundsechzig
* das bekannte Spiel Sk... King (hier Piraten)
* das bekannte Spiel Wi..rd (hier Zauberer)
* das Würfelspiel Kn...el (hier Pasch)
* und alle anderen Spiele mit einfacher Wertung duch Addition/Subtraktion

## Features
* unterstützte Sprachen: englisch, deutsch
* bis zu 8 Spieler (je nach Spiel)
* unbegrenzte Anzahl vordefinierter Spielernamen
* Rommé: doppelte Wertung bei Hand-Rommé mit einem Klick
* automatische Berechnung
* Markierung für den Geber
* **keine Berechtigungen erforderlich**

## Screenshots
<img src="fastlane/metadata/android/de-DE/images/phoneScreenshots/1.png" alt="Screenshot Startbildschirm" width="200"/>
<img src="fastlane/metadata/android/de-DE/images/phoneScreenshots/2.png" alt="Screenshot Sechsundsechzig" width="200"/>
<img src="fastlane/metadata/android/de-DE/images/phoneScreenshots/3.png" alt="Screenshot Piraten" width="200"/>
<img src="fastlane/metadata/android/de-DE/images/phoneScreenshots/4.png" alt="Screenshot Pasch" width="200"/>

## Remarks
Diese Android-App wurde für eigene private Zwecke erstellt, um die "Zettelwirtschaft" zu beseitigen. Vielleicht ist sie auch für andere nützlich.

**Sollten Sie einen Verstoß gegen Markenrechte entdecken, öffnen Sie bitte einen Issue.**