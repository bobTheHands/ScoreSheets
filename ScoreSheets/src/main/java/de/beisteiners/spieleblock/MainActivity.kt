/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Ronny Steiner
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.beisteiners.spieleblock

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Typeface
import android.media.MediaPlayer
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.MenuCompat
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.content_yahtzee.*
import kotlinx.android.synthetic.main.spinner_bids.*
import kotlin.math.abs
import kotlin.math.sqrt
import android.widget.LinearLayout

const val welcome = 0
const val rummy = 1
const val sixtySix = 2
const val skullKing = 3
const val wizard = 4
const val addition = 5
const val addition2 = 6
const val yahtzee = 7
var game = welcome
var game_over = false
const val countNormal = 0
const val countHandRummy = 1
var playersArray = arrayOf<EditText>()
var placesArray = arrayOf<TextView>()
var namesArray = arrayOf<Spinner>()
var totalsArray = arrayOf<TextView>()
var rowsArray = arrayOf<LinearLayout>()
var bidsArray = arrayOf<Array<Spinner>>()
var tricksArray = arrayOf<Array<Spinner>>()
var bonusArray = arrayOf<Array<Spinner>>()
var inputArray = arrayOf<Array<EditText>>()
var scoreArray = arrayOf<Array<TextView>>()
var textSizePlayerNames = 12f

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        game = welcome
        playersArray = arrayOf()
        placesArray = arrayOf()
        namesArray = arrayOf()
        totalsArray = arrayOf()
        rowsArray = arrayOf()
        bidsArray = arrayOf()
        tricksArray = arrayOf()
        bonusArray = arrayOf()
        inputArray = arrayOf()
        scoreArray = arrayOf()
    }

    override fun onResume() {
        super.onResume()
        content_main.keepScreenOn=PreferenceManager.getDefaultSharedPreferences(this).getBoolean("pref_keep_screen_on", true)
        if (game > 0 && game < 7 && rowsArray.isEmpty() && playersArray.isNotEmpty()) {
            displayPlayer()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        MenuCompat.setGroupDividerEnabled(menu, true)

        content_main.keepScreenOn=PreferenceManager.getDefaultSharedPreferences(this).getBoolean("pref_keep_screen_on", true)
        initializeGame()
        setTextSizes()

        return true
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        setTextSizes()
    }

    override fun onBackPressed() {
//        super.onBackPressed()
        if (game == welcome) {
//            AlertDialog.Builder(this)
//                .setMessage(getString(R.string.exit_question))
//                .setNegativeButton(R.string.no, null)
//                .setPositiveButton(R.string.yes, object : DialogInterface.OnClickListener {
//                    override fun onClick(arg0: DialogInterface?, arg1: Int) {
                        super@MainActivity.onBackPressed()
//                    }
//                }).create().show()
        } else {
            alertCancelGame(welcome,false)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val imm: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(game_sheet.getWindowToken(), 0)
        when (item.itemId) {
            R.id.action_new_game -> {
                alertCancelGame(game,false)

                when (game) {
                    rummy, sixtySix, skullKing, wizard, addition, addition2 -> {
                        player_names_spinner.visibility = View.GONE
                        footer.visibility = View.GONE
                        totals.visibility = View.VISIBLE
                        addRow(countNormal)
                    }
                    yahtzee -> {
                    }
                }
            }
            R.id.action_add_player -> {
                addPlayer()
            }
            R.id.action_remove_player -> {
                removePlayer()
            }
            R.id.action_start_game -> {
                for (i in 0..playersArray.size-2) {
                    for (j in i+1..playersArray.size-1) {
                        if (playersArray[i].text.toString().isNotEmpty() && playersArray[i].text.toString() == playersArray[j].text.toString()) {
                            showSnackbar(R.string.snackbar_same_names, Snackbar.LENGTH_SHORT,"")
                            return false
                        }
                    }
                }
                val imm: InputMethodManager =
                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(player_names.windowToken, 0)
                player_names_spinner.visibility = View.GONE
                footer.visibility = View.GONE
                totals.visibility = View.VISIBLE

                when (game) {
                    rummy -> {
                        addRow(countNormal)
                    }
                    sixtySix -> {
                        addRow(countNormal)
                    }
                    skullKing -> {
                        addRow(countNormal)
                    }
                    wizard -> {
                        addRow(countNormal)
                    }
                    addition, addition2 -> {
                        addRow(countNormal)
                    }
                }
            }
            R.id.action_add_row_normal -> {
                when (game) {
                    rummy -> {
                        if ( checkLogic() ) addRow(countNormal)
                    }
                    skullKing -> {
                        if ( checkLogic() ) addRow(countNormal)
                    }
                    wizard -> {
                        if ( checkLogic() ) addRow(countNormal)
                    }
                    addition, addition2 -> {
                        if ( checkLogic() ) addRow(countNormal)
                    }
                }
            }
            R.id.action_add_row_double -> {
                when (game) {
                    rummy -> {
                        if ( checkLogic() ) addRow(countHandRummy)
                    }
                }
            }
            R.id.action_remove_round -> {
                when (game) {
                    rummy -> {
                        inputArray = inputArray.dropLast(1).toTypedArray()
                        rowsArray = rowsArray.dropLast(1).toTypedArray()
                        for (i in playersArray.indices) {
                            if (inputArray[rowsArray.size-1][i].tag==2) {
                                inputArray[rowsArray.size-1][i].tag=1
                                inputArray[rowsArray.size-1][i].setText((inputArray[rowsArray.size-1][i].text.toString().toInt()/2).toString())
                            }
                        }
                        displayScores()
                    }
                    sixtySix -> {
                        placesArray[0].text= placesArray[0].text.toString().dropLast(3)
                        placesArray[1].text= placesArray[1].text.toString().dropLast(3)
                        displayScores()
                    }
                    skullKing -> {
                        bidsArray = bidsArray.dropLast(1).toTypedArray()
                        tricksArray = tricksArray.dropLast(1).toTypedArray()
                        scoreArray = scoreArray.dropLast(1).toTypedArray()
                        rowsArray = rowsArray.dropLast(1).toTypedArray()
                        displayScores()
                    }
                    wizard -> {
                        bidsArray = bidsArray.dropLast(1).toTypedArray()
                        tricksArray = tricksArray.dropLast(1).toTypedArray()
                        scoreArray = scoreArray.dropLast(1).toTypedArray()
                        rowsArray = rowsArray.dropLast(1).toTypedArray()
                        displayScores()
                    }
                    addition, addition2 -> {
                        inputArray = inputArray.dropLast(1).toTypedArray()
                        rowsArray = rowsArray.dropLast(1).toTypedArray()
                        displayScores()
                    }
                }
            }
            R.id.action_play_sound -> {
                val mp: MediaPlayer
                if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean("pref_skull_yohoho_only", false)) {
                    when ((Math.random() * 100).toInt() % 11) {
                        0 -> {
                            mp = MediaPlayer.create(this, R.raw.einszweidrei)
                        }
                        1 -> {
                            mp = MediaPlayer.create(this, R.raw.enemenemuh)
                        }
                        2 -> {
                            mp = MediaPlayer.create(this, R.raw.hundkatzemaus)
                        }
                        3 -> {
                            mp = MediaPlayer.create(this, R.raw.rasdwatri)
                        }
                        4 -> {
                            mp = MediaPlayer.create(this, R.raw.ringelrangelrose)
                        }
                        5 -> {
                            mp = MediaPlayer.create(this, R.raw.rirarutsch)
                        }
                        6 -> {
                            mp = MediaPlayer.create(this, R.raw.schnickschnackschnuck)
                        }
                        7 -> {
                            mp = MediaPlayer.create(this, R.raw.vatermutterkind)
                        }
                        else -> {
                            mp = MediaPlayer.create(this, R.raw.yohoho)
                        }
                    }
                } else {
                    mp = MediaPlayer.create(this, R.raw.yohoho)
                }
                mp.start()
            }
            R.id.action_end_the_game -> {
                gameOver()
            }
            R.id.action_reset_game -> {
                alertCancelGame(game, true)
            }
            R.id.action_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_license -> {
                val intent = Intent(this, LicenseActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.action_about -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    fun startRummy(view: View) {
        game = rummy
        initializeGame()
    }

    fun startSixtySix(view: View) {
        game = sixtySix
        initializeGame()
    }
    fun startSkull(view: View) {
        game = skullKing
        initializeGame()
    }

    fun startWizard(view: View) {
        game = wizard
        initializeGame()
    }

    fun startAddition(view: View) {
        game = addition
        initializeGame()
    }

    fun startAddition2(view: View) {
        game = addition2
        initializeGame()
    }

    fun startYahtzee(view: View) {
        game = yahtzee
        initializeGame()
    }

    fun yahtzeeFullHouse(view: View) {
        var viewTmp: TextView = findViewById(view.id)
        content_yahtzee.clearFocus()
        val imm: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
        when (viewTmp.text.toString()) {
            "25" -> viewTmp.setText("X")
            "X" -> viewTmp.setText("")
            else -> viewTmp.setText("25")
        }
        calcYahtzee(view.id)
    }

    fun yahtzeeSmallStraight(view: View) {
        var viewTmp: TextView = findViewById(view.id)
        content_yahtzee.clearFocus()
        val imm: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
        when (viewTmp.text.toString()) {
            "30" -> viewTmp.setText("X")
            "X" -> viewTmp.setText("")
            else -> viewTmp.setText("30")
        }
        calcYahtzee(view.id)
    }

    fun yahtzeeLargeStraight(view: View) {
        var viewTmp: TextView = findViewById(view.id)
        content_yahtzee.clearFocus()
        val imm: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
        when (viewTmp.text.toString()) {
            "40" -> viewTmp.setText("X")
            "X" -> viewTmp.setText("")
            else -> viewTmp.setText("40")
        }
        calcYahtzee(view.id)
    }

    fun yahtzeeYahtzee(view: View) {
        var viewTmp: TextView = findViewById(view.id)
        content_yahtzee.clearFocus()
        val imm: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0)
        when (viewTmp.text.toString()) {
            "50" -> viewTmp.setText("X")
            "X" -> viewTmp.setText("")
            else -> viewTmp.setText("50")
        }
        calcYahtzee(view.id)
    }

    fun yahtzeeHelp(view: View) {
        val builder = AlertDialog.Builder(this)
        with(builder)
        {
            when (view.id) {
                yahtzeeImage1.id -> {
                    setTitle(R.string.yahtzee_1)
                    setMessage(R.string.yahtzeeHelp_1)
                }
                yahtzeeImage2.id -> {
                    setTitle(R.string.yahtzee_2)
                    setMessage(R.string.yahtzeeHelp_2)
                }
                yahtzeeImage3.id -> {
                    setTitle(R.string.yahtzee_3)
                    setMessage(R.string.yahtzeeHelp_3)
                }
                yahtzeeImage4.id -> {
                    setTitle(R.string.yahtzee_4)
                    setMessage(R.string.yahtzeeHelp_4)
                }
                yahtzeeImage5.id -> {
                    setTitle(R.string.yahtzee_5)
                    setMessage(R.string.yahtzeeHelp_5)
                }
                yahtzeeImage6.id -> {
                    setTitle(R.string.yahtzee_6)
                    setMessage(R.string.yahtzeeHelp_6)
                }
                yahtzeeTotal1.id -> {
                    setTitle(R.string.yahtzeeHelp_Total1)
                    setMessage(R.string.yahtzeeHelp_total1)
                }
                yahtzeeImage3k.id -> {
                    setTitle(R.string.yahtzee_3_of_a_kind)
                    setMessage(R.string.yahtzeeHelp_3k)
                }
                yahtzeeImage4k.id -> {
                    setTitle(R.string.yahtzee_4_of_a_kind)
                    setMessage(R.string.yahtzeeHelp_4k)
                }
                yahtzeeImageFH.id -> {
                    setTitle(R.string.yahtzee_full_house)
                    setMessage(R.string.yahtzeeHelp_FH)
                }
                yahtzeeImageSS.id -> {
                    setTitle(R.string.yahtzee_small_straight)
                    setMessage(R.string.yahtzeeHelp_SS)
                }
                yahtzeeImageLS.id -> {
                    setTitle(R.string.yahtzee_large_straight)
                    setMessage(R.string.yahtzeeHelp_LS)
                }
                yahtzeeImageY.id -> {
                    setTitle(R.string.yahtzee_yahtzee)
                    setMessage(R.string.yahtzeeHelp_Y)
                }
                yahtzeeImageC.id -> {
                    setTitle(R.string.yahtzee_chance)
                    setMessage(R.string.yahtzeeHelp_C)
                }
                yahtzeeTotal2.id -> {
                    setTitle(R.string.yahtzeeHelp_Total2)
                    setMessage(R.string.yahtzeeHelp_total2)
                }
                yahtzeeTotal.id -> {
                    setTitle(R.string.yahtzeeHelp_Total)
                    setMessage(R.string.yahtzeeHelp_total)
                }
                else -> {
                    setTitle("Title")
                    setMessage("Description")
                }
            }
            setPositiveButton("OK", null)
            show()
        }
    }

    private fun alertCancelGame(gametmp: Int, reset: Boolean) {
        if ( !game_over && ((rowsArray.isNotEmpty() && player_places.visibility == TextView.GONE) || (game == sixtySix && rowsArray.size>0) || (game == yahtzee && !calcYahtzee(0))) ) {
            AlertDialog.Builder(this)
//                .setTitle("Really Exit?")
                .setMessage(getString(R.string.cancel_game_question))
                .setNegativeButton(R.string.no, null)
                .setPositiveButton(R.string.yes, object : DialogInterface.OnClickListener {
                    override fun onClick(arg0: DialogInterface?, arg1: Int) {
                        if (reset) {
                            playersArray = arrayOf()
                            namesArray = arrayOf()
                            placesArray = arrayOf()
                            totalsArray = arrayOf()
                        }
                        rowsArray = arrayOf()
                        bidsArray = arrayOf()
                        tricksArray = arrayOf()
                        scoreArray = arrayOf()
                        bonusArray = arrayOf()
                        game = gametmp
                        initializeGame()
                    }
                }).create().show()
        } else {
            if (reset) {
                playersArray = arrayOf()
                namesArray = arrayOf()
                placesArray = arrayOf()
                totalsArray = arrayOf()
            }
            rowsArray = arrayOf()
            bidsArray = arrayOf()
            tricksArray = arrayOf()
            scoreArray = arrayOf()
            bonusArray = arrayOf()
            game = gametmp
            initializeGame()
        }
        return
    }

    private fun gameOver() {
        when (game) {
            rummy -> {
                var noInputs = true
                for (i in playersArray.indices) {
                    if ( inputArray[rowsArray.size-1][i].text.isNotEmpty() ) noInputs = false
                }
                if (noInputs) {
                    inputArray = inputArray.dropLast(1).toTypedArray()
                    rowsArray = rowsArray.dropLast(1).toTypedArray()
                    displayScores()
                }
                if (checkLogic()) {
                    for (i in playersArray.indices) {
                        if (inputArray[rowsArray.size - 1][i].tag == 2) {
                            inputArray[rowsArray.size - 1][i].background = getDrawable(R.drawable.hand_rummy)
                        } else {
                            inputArray[rowsArray.size - 1][i].background = getDrawable(R.drawable.score_display)
                        }
                        inputArray[rowsArray.size - 1][i].isFocusable = false
                    }
                    game_over = true
                    showPlaces()
                    updateToolbar(getString(R.string.action_game_rummy), getString(R.string.game_over),0,0,0,0,0,0,0,2,0, 2)
                }
            }
            sixtySix -> {
                game_over = true
                showPlaces()
                updateToolbar(getString(R.string.action_game_66), getString(R.string.game_over),0,0,0,0,0,0,0, 2,0, 2)
            }
            skullKing -> {
                if (checkLogic()) {
                    for (i in playersArray.indices) {
                        bidsArray[rowsArray.size - 1][i].visibility = Spinner.GONE
                        tricksArray[rowsArray.size - 1][i].visibility =Spinner.GONE
                        scoreArray[rowsArray.size - 1][i].background = getDrawable(R.drawable.score_display)
                        bonusArray[rowsArray.size - 1][i].visibility =Spinner.GONE
                    }
                    game_over = true
                    showPlaces()
                    updateToolbar(getString(R.string.action_game_skull), getString(R.string.game_over),0,0,0,0,0,0,0, 2,0, 2)
                }
            }
            wizard -> {
                if (checkLogic()) {
                    for (i in playersArray.indices) {
                        bidsArray[rowsArray.size - 1][i].visibility = Spinner.GONE
                        tricksArray[rowsArray.size - 1][i].visibility =Spinner.GONE
                        scoreArray[rowsArray.size - 1][i].background = getDrawable(R.drawable.score_display)
                    }
                    game_over = true
                    showPlaces()
                    updateToolbar(getString(R.string.action_game_wizard), getString(R.string.game_over),0,0,0,0,0,0,0,2,0, 2)
                }
            }
            addition -> {
                var noInputs = true
                for (i in playersArray.indices) {
                    if ( inputArray[rowsArray.size-1][i].text.isNotEmpty() ) noInputs = false
                }
                if (noInputs) {
                    inputArray = inputArray.dropLast(1).toTypedArray()
                    rowsArray = rowsArray.dropLast(1).toTypedArray()
                    displayScores()
                }
                if (checkLogic()) {
                    for (i in playersArray.indices) {
                        inputArray[rowsArray.size - 1][i].background = getDrawable(R.drawable.score_display)
                        inputArray[rowsArray.size - 1][i].isFocusable = false
                    }
                    game_over = true
                    showPlaces()
                    updateToolbar(getString(R.string.action_game_addition), getString(R.string.game_over),0,0,0,0,0,0,0,2,0, 2)
                }
            }
            addition2 -> {
                var noInputs = true
                for (i in playersArray.indices) {
                    if ( inputArray[rowsArray.size-1][i].text.isNotEmpty() ) noInputs = false
                }
                if (noInputs) {
                    inputArray = inputArray.dropLast(1).toTypedArray()
                    rowsArray = rowsArray.dropLast(1).toTypedArray()
                    displayScores()
                }
                if (checkLogic()) {
                    for (i in playersArray.indices) {
                        inputArray[rowsArray.size - 1][i].background = getDrawable(R.drawable.score_display)
                        inputArray[rowsArray.size - 1][i].isFocusable = false
                    }
                    game_over = true
                    showPlaces()
                    updateToolbar(getString(R.string.action_game_addition2), getString(R.string.game_over),0,0,0,0,0,0,0,2,0, 2)
                }
            }
        }
        if (game_over) {
            totals.visibility = View.GONE
            footer.visibility = View.VISIBLE
            setTextSizes()
        }
        val imm: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(player_names.windowToken, 0)
    }

    private fun showPlaces() {
        calcTotals()
        if (game != sixtySix) {
            var scores = arrayOf<Int>()
            var scoresNew = arrayOf<Int>()
            var places = arrayOf<Int>()
            var place = 0
            for (i in playersArray.indices) {
                scores += totalsArray[i].text.toString().toInt()
            }
            when (game) {
                rummy -> {
                    scores.sort()
                    for (i in scores.indices) {
                        scoresNew += scores[i]
                        place++
                        places += place
                    }
                }
                addition -> {
                    scores.sortDescending()
                    for (i in scores.indices) {
                        scoresNew += scores[i]
                        place++
                        places += place
                    }
                }
                addition2 -> {
                    scores.sort()
                    for (i in scores.indices) {
                        scoresNew += scores[i]
                        place++
                        places += place
                    }
                }
                skullKing, wizard -> {
                    scores.sortDescending()
                    for (i in scores.indices) {
                        scoresNew += scores[i]
                        place++
                        places += place
                    }
                }
            }
            for (i in playersArray.indices) {
                placesArray[i].text = getString(
                    R.string.place,
                    places[scoresNew.indexOf(totalsArray[i].text.toString().toInt())], totalsArray[i].text.toString().toInt())
                playersArray[i].background = getDrawable(R.drawable.player_input)
            }
            player_places.visibility = TextView.VISIBLE
        } else {
            if (placesArray[0].tag.toString().toInt() > placesArray[1].tag.toString().toInt()) {
                placesArray[0].text = getString(R.string.place, 1, placesArray[0].tag.toString().toInt())
                placesArray[1].text = getString(R.string.place, 2, placesArray[1].tag.toString().toInt())
            } else if (placesArray[0].tag.toString().toInt() < placesArray[1].tag.toString().toInt()) {
                placesArray[0].text = getString(R.string.place, 2, placesArray[0].tag.toString().toInt())
                placesArray[1].text = getString(R.string.place, 1, placesArray[1].tag.toString().toInt())
            } else {
                placesArray[0].text = getString(R.string.place, 1, placesArray[0].tag.toString().toInt())
                placesArray[1].text = getString(R.string.place, 1, placesArray[1].tag.toString().toInt())
            }
            totals.removeAllViews()
        }
    }

    private fun addRow(countMode: Int) {
        val latestRowIndex = rowsArray.size
        val paddingHorizontal = dp2px(3f)
        val paddingVertical = dp2px(2f)
        val rowParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        rowsArray += LinearLayout(game_sheet.context)
        rowsArray[latestRowIndex].layoutParams = rowParams
        rowsArray[latestRowIndex].orientation = LinearLayout.HORIZONTAL
        rowsArray[latestRowIndex].setPadding(paddingHorizontal,paddingVertical,paddingHorizontal,paddingVertical)
        if (latestRowIndex==0) {
            for (i in playersArray.indices) {
                playersArray[i].isFocusable = false
                if (playersArray[i].text.isEmpty()) playersArray[i].setText(playersArray[i].hint)
            }
        }


        when (game) {
            rummy -> {
                var inputArrayTmp = arrayOf<EditText>()
                for (i in playersArray.indices) {
                    if (latestRowIndex>0) {
                        if (countMode == countHandRummy) {
                            inputArray[inputArray.size - 1][i].tag=2
                            inputArray[inputArray.size - 1][i].setText(
                                (inputArray[inputArray.size - 1][i].text.toString()
                                    .toInt() * 2).toString()
                            )
                        } else {
                            inputArray[inputArray.size - 1][i].tag=1
                        }
                    }
                    val rowPlayer = newRowPlayer(rowsArray[latestRowIndex])
                    rowsArray[latestRowIndex].addView(rowPlayer)
                    inputArrayTmp += newInput(rowPlayer, i)
                    rowPlayer.addView(inputArrayTmp[i])
                }
                inputArray += inputArrayTmp
            }
            sixtySix -> {
                var scoreArrayTmp = arrayOf<TextView>()
                for (i in playersArray.indices) {
                    val rowPlayer = newRowPlayer(rowsArray[latestRowIndex])
                    rowsArray[latestRowIndex].addView(rowPlayer)
                    scoreArrayTmp += newTallyMarks(rowPlayer)
                    rowPlayer.addView(scoreArrayTmp[i])
                }
                scoreArray += scoreArrayTmp
                val buttonView: View = layoutInflater.inflate(R.layout.sixtysix_button, null)
                totals.addView(buttonView)
                findViewById<Button>(R.id.sixtysix_p1_1).typeface = Typeface.createFromAsset(applicationContext.assets, "tally.ttf")
                findViewById<Button>(R.id.sixtysix_p1_2).typeface = Typeface.createFromAsset(applicationContext.assets, "tally.ttf")
                findViewById<Button>(R.id.sixtysix_p1_3).typeface = Typeface.createFromAsset(applicationContext.assets, "tally.ttf")
                findViewById<Button>(R.id.sixtysix_p2_1).typeface = Typeface.createFromAsset(applicationContext.assets, "tally.ttf")
                findViewById<Button>(R.id.sixtysix_p2_2).typeface = Typeface.createFromAsset(applicationContext.assets, "tally.ttf")
                findViewById<Button>(R.id.sixtysix_p2_3).typeface = Typeface.createFromAsset(applicationContext.assets, "tally.ttf")
            }
            skullKing -> {
                var bidsArrayTmp = arrayOf<Spinner>()
                var tricksArrayTmp = arrayOf<Spinner>()
                var scoreArrayTmp = arrayOf<TextView>()
                var bonusArrayTmp = arrayOf<Spinner>()
                for (i in playersArray.indices) {
                    val rowPlayer = newRowPlayer(rowsArray[latestRowIndex])
                    rowsArray[latestRowIndex].addView(rowPlayer)
                    bidsArrayTmp += newBid(rowPlayer)
                    tricksArrayTmp += newTricks(rowPlayer)
                    scoreArrayTmp += newScore(rowPlayer)
                    bonusArrayTmp += newBonus(rowPlayer)
                    rowPlayer.addView(bidsArrayTmp[i])
                    rowPlayer.addView(tricksArrayTmp[i])
                    rowPlayer.addView(scoreArrayTmp[i])
                    rowPlayer.addView(bonusArrayTmp[i])
                }
                bidsArray += bidsArrayTmp
                tricksArray += tricksArrayTmp
                scoreArray += scoreArrayTmp
                bonusArray += bonusArrayTmp
            }
            wizard -> {
                var bidsArrayTmp = arrayOf<Spinner>()
                var tricksArrayTmp = arrayOf<Spinner>()
                var scoreArrayTmp = arrayOf<TextView>()
                for (i in playersArray.indices) {
                    val rowPlayer = newRowPlayer(rowsArray[latestRowIndex])
                    rowsArray[latestRowIndex].addView(rowPlayer)
                    bidsArrayTmp += newBid(rowPlayer)
                    tricksArrayTmp += newTricks(rowPlayer)
                    scoreArrayTmp += newScore(rowPlayer)
                    rowPlayer.addView(bidsArrayTmp[i])
                    rowPlayer.addView(tricksArrayTmp[i])
                    rowPlayer.addView(scoreArrayTmp[i])
                }
                bidsArray += bidsArrayTmp
                tricksArray += tricksArrayTmp
                scoreArray += scoreArrayTmp
            }
            addition, addition2 -> {
                var inputArrayTmp = arrayOf<EditText>()
                for (i in playersArray.indices) {
                    if (latestRowIndex>0) {
                        inputArray[inputArray.size - 1][i].tag=1
                    }
                    val rowPlayer = newRowPlayer(rowsArray[latestRowIndex])
                    rowsArray[latestRowIndex].addView(rowPlayer)
                    inputArrayTmp += newInput(rowPlayer, i)
                    rowPlayer.addView(inputArrayTmp[i])
                }
                inputArray += inputArrayTmp
            }
        }
        displayScores()
        calcTotals()
        rowsArray[latestRowIndex].requestFocus()
    }

    private fun newRowPlayer(parentView: View): LinearLayout {
        val marginHorizontal = dp2px(2f)
        val rowPlayerParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f)
        rowPlayerParams.setMargins(marginHorizontal,0,marginHorizontal,0)
        val rowPlayer = LinearLayout(parentView.context)
        rowPlayer.layoutParams = rowPlayerParams
        rowPlayer.orientation = LinearLayout.VERTICAL
        return rowPlayer
    }

    private fun newInput(view: View, player: Int): EditText {
        val newInput = EditText(view.context)
        val padding = dp2px(5f)
        newInput.setPadding(padding, padding, padding, padding)
        newInput.background = resources.getDrawable(R.drawable.score_input_normal, this.theme)
        newInput.textAlignment = View.TEXT_ALIGNMENT_CENTER
        newInput.setSelectAllOnFocus(true)
        newInput.hint = "("+ playersArray[player].text+")"
        if (player < playersArray.size-1) newInput.imeOptions=EditorInfo.IME_ACTION_NEXT
        when (game) {
            addition, addition2 -> {
                newInput.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED
            }
            else -> {
                newInput.inputType = InputType.TYPE_CLASS_NUMBER
            }
        }
        newInput.textSize = 16f
        newInput.tag = 1

        val textWatcher: TextWatcher? = object : TextWatcher {
            var ignoreChange=false
            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                if (!ignoreChange && newInput.isFocusable){
                    ignoreChange=true
                    newInput.setText(s.replaceFirst("^0+(?!$)".toRegex(), "").replaceFirst("^-0+(?!$)".toRegex(), "-"))
                    if (s.length > 2 && newInput.tag == 1) {
                        if (s.toString().toInt() > 999) {
                            newInput.setText(s.subSequence(0, s.length - 1))
                            showSnackbar(R.string.snackbar_scorelimit, Snackbar.LENGTH_LONG,"")
                        }
                        if (s.toString().toInt() < -999) {
                            newInput.setText(s.subSequence(0, s.length - 1))
                            showSnackbar(R.string.snackbar_scorelimitmin, Snackbar.LENGTH_LONG,"")
                        }
                    }
                    newInput.setSelection(newInput.text.length)
                    ignoreChange=false
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub
            }

            override fun afterTextChanged(s: Editable) {
                // TODO Auto-generated method stub
            }
        }
        newInput.addTextChangedListener(textWatcher)

        return newInput
    }

    private fun newBid(view: View): Spinner {
        val newBid = Spinner(view.context)
        val newBidParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        newBidParams.topMargin = dp2px(1f)
        newBidParams.bottomMargin = dp2px(1f)
        newBid.layoutParams = newBidParams
        newBid.background = getDrawable(R.drawable.score_input_bids)
        val bidAdapter = object : ArrayAdapter<CharSequence?>(
            this,
            R.layout.spinner_bids_item, ArrayList<String>() as List<CharSequence?>
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            // Change color item
            override fun getDropDownView(
                position: Int, convertView: View?,
                parent: ViewGroup
            ): View? {
                // TODO Auto-generated method stub
                val mView = super.getDropDownView(position, convertView, parent)
                val mTextView = mView as TextView
                if (position == 0) {
                    mTextView.setTextColor(ContextCompat.getColor(context, R.color.colorHints))
                    mTextView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBidHintBackground))
                } else {
                    mTextView.setTextColor(ContextCompat.getColor(context, R.color.colorBidText))
                    mTextView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBidBackground))
                }
                return mView
            }
        }
        bidAdapter.add(resources.getString(R.string.bid))
        for (i in 0..rowsArray.size) {
            bidAdapter.add(i.toString())
        }
        bidAdapter.setDropDownViewResource(R.layout.spinner_bids)
        newBid.adapter = bidAdapter
        newBid.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                val selectedItem =
                    parent.getItemAtPosition(position).toString()
                checkAllBids()
                calcScore()
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        return newBid
    }

    private fun checkAllBids() {
        val countBidSum = checkBids()
        if (countBidSum==-1) {
//            showSnackbar(R.string.snackbar_empty_bids, Snackbar.LENGTH_SHORT)
//            return false
        } else if (game== wizard && countBidSum == rowsArray.size && !PreferenceManager.getDefaultSharedPreferences(this).getBoolean("pref_wizard_even_bids", true)) {
            showSnackbar(R.string.snackbar_even_bids_not_allowed, Snackbar.LENGTH_LONG,"")
        } else {
            for (i in playersArray.indices) {
                tricksArray[rowsArray.size-1][i].visibility = Spinner.VISIBLE
                scoreArray[rowsArray.size-1][i].visibility = Spinner.VISIBLE
                scroll_view.post {
                    scroll_view.fullScroll(View.FOCUS_DOWN)
                }
            }
        }
    }

    private fun newTricks(view: View): Spinner {
        val newTricks = Spinner(view.context)
        val newTricksParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        newTricksParams.topMargin = dp2px(1f)
        newTricksParams.bottomMargin = dp2px(1f)
        newTricks.layoutParams = newTricksParams
        newTricks.background = getDrawable(R.drawable.score_input_tricks)
        val tricksAdapter = object : ArrayAdapter<CharSequence?>(
            this,
            R.layout.spinner_tricks_item, ArrayList<String>() as List<CharSequence?>
        ) {
            override fun isEnabled(position: Int): Boolean {
                return position != 0
            }

            // Change color item
            override fun getDropDownView(
                position: Int, convertView: View?,
                parent: ViewGroup
            ): View? {
                // TODO Auto-generated method stub
                val mView = super.getDropDownView(position, convertView, parent)
                val mTextView = mView as TextView
                if (position == 0) {
                    mTextView.setTextColor(ContextCompat.getColor(context, R.color.colorHints))
                    mTextView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorTricksHintBackground))
                } else {
                    mTextView.setTextColor(ContextCompat.getColor(context, R.color.colorTricksText))
                    mTextView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorTricksBackground))
                }
                return mView
            }
        }
        tricksAdapter.add(resources.getString(R.string.tricks))
        for (i in 0..rowsArray.size) {
            tricksAdapter.add(i.toString())
        }
        tricksAdapter.setDropDownViewResource(R.layout.spinner_tricks)
        newTricks.adapter = tricksAdapter
        newTricks.visibility = Spinner.GONE
        newTricks.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                val selectedItem =
                    parent.getItemAtPosition(position).toString()
                calcScore()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        return newTricks
    }

    private fun newBonus(view: View): Spinner {
        val newBonus = Spinner(view.context)
        val newBonusParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        newBonusParams.topMargin = dp2px(1f)
        newBonusParams.bottomMargin = dp2px(1f)
        newBonus.layoutParams = newBonusParams
        newBonus.background = getDrawable(R.drawable.score_input_bonus)
        val bonusAdapter = object : ArrayAdapter<CharSequence?>(
            this,
            R.layout.spinner_bonus_item, ArrayList<String>() as List<CharSequence?>
        ) {
            override fun isEnabled(position: Int): Boolean {
                return true
            }

            // Change color item
            override fun getDropDownView(
                position: Int, convertView: View?,
                parent: ViewGroup
            ): View? {
                // TODO Auto-generated method stub
                val mView = super.getDropDownView(position, convertView, parent)
                val mTextView = mView as TextView
                if (position == 0) {
                    mTextView.setTextColor(ContextCompat.getColor(context, R.color.colorHints))
                    mTextView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBonusHintBackground))
                } else {
                    mTextView.setTextColor(ContextCompat.getColor(context, R.color.colorBonusText))
                    mTextView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBonusBackground))
                }
                return mView
            }
        }

        bonusAdapter.add(resources.getString(R.string.bonus))
        val skullBonus = resources.getStringArray(R.array.skull_bonus)
        for (i in 0..playersArray.size-1) {
            bonusAdapter.add(skullBonus[i])
        }
        bonusAdapter.setDropDownViewResource(R.layout.spinner_bonus)
        newBonus.adapter = bonusAdapter
        newBonus.visibility = Spinner.GONE
        newBonus.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                val selectedItem =
                    parent.getItemAtPosition(position).toString()
                calcScore()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        return newBonus
    }

    private fun newTallyMarks(view: View): TextView {
        val padding = dp2px(5f)
        val newTally = TextView(view.context)
        val newTallyParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        newTallyParams.topMargin = dp2px(1f)
        newTallyParams.bottomMargin = dp2px(1f)
        newTally.layoutParams = newTallyParams
        newTally.setPadding(padding, padding, padding, padding)
        newTally.textAlignment = View.TEXT_ALIGNMENT_CENTER
        newTally.setTextColor(ContextCompat.getColor(this, R.color.colorResultsText))
        newTally.typeface = Typeface.createFromAsset(applicationContext.assets, "tally.ttf")
        newTally.textSize = 16f
        return newTally
    }

    fun sixtysixP1Add1(view: View) {
        if (placesArray[0].text.isEmpty()) {
            placesArray[0].text = "1"
            placesArray[1].text = "0"
        } else {
            placesArray[0].text = placesArray[0].text.toString() + ", 1"
            placesArray[1].text = placesArray[1].text.toString() + ", 0"
        }
        displayScores()
    }

    fun sixtysixP1Add2(view: View) {
        if (placesArray[0].text.isEmpty()) {
            placesArray[0].text = "2"
            placesArray[1].text = "0"
        } else {
            placesArray[0].text = placesArray[0].text.toString() + ", 2"
            placesArray[1].text = placesArray[1].text.toString() + ", 0"
        }
        displayScores()
    }

    fun sixtysixP1Add3(view: View) {
        if (placesArray[0].text.isEmpty()) {
            placesArray[0].text = "3"
            placesArray[1].text = "0"
        } else {
            placesArray[0].text = placesArray[0].text.toString() + ", 3"
            placesArray[1].text = placesArray[1].text.toString() + ", 0"
        }
        displayScores()
    }

    fun sixtysixP2Add1(view: View) {
        if (placesArray[1].text.isEmpty()) {
            placesArray[0].text = "0"
            placesArray[1].text = "1"
        } else {
            placesArray[0].text = placesArray[0].text.toString() + ", 0"
            placesArray[1].text = placesArray[1].text.toString() + ", 1"
        }
        displayScores()
    }

    fun sixtysixP2Add2(view: View) {
        if (placesArray[1].text.isEmpty()) {
            placesArray[0].text = "0"
            placesArray[1].text = "2"
        } else {
            placesArray[0].text = placesArray[0].text.toString() + ", 0"
            placesArray[1].text = placesArray[1].text.toString() + ", 2"
        }
        displayScores()
    }

    fun sixtysixP2Add3(view: View) {
        if (placesArray[1].text.isEmpty()) {
            placesArray[0].text = "0"
            placesArray[1].text = "3"
        } else {
            placesArray[0].text = placesArray[0].text.toString() + ", 0"
            placesArray[1].text = placesArray[1].text.toString() + ", 3"
        }
        displayScores()
    }

    private fun newScore(view: View): TextView {
        val padding = dp2px(5f)
        val newScore = TextView(view.context)
        val newScoreParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        newScoreParams.topMargin = dp2px(1f)
        newScoreParams.bottomMargin = dp2px(1f)
        newScore.layoutParams = newScoreParams
        newScore.setPadding(padding, padding, padding, padding)
        newScore.background = resources.getDrawable(R.drawable.score_input_normal_wizard, this.theme)
        newScore.textAlignment = View.TEXT_ALIGNMENT_CENTER
        newScore.textSize = 16f
        newScore.setTextColor(ContextCompat.getColor(this, R.color.colorResultsText))
        newScore.typeface = Typeface.DEFAULT_BOLD
        newScore.hint = resources.getString(R.string.hint_wizard_score)
        newScore.visibility = TextView.GONE
        return newScore
    }

    private fun calcScore() {
        when (game) {
            skullKing -> {
                for (i in playersArray.indices) {
                    if ( bidsArray[rowsArray.size-1][i].selectedItemPosition > 0 && tricksArray[rowsArray.size-1][i].selectedItemPosition > 0 ) {
                        if (bidsArray[rowsArray.size-1][i].selectedItemPosition == 1 && tricksArray[rowsArray.size-1][i].selectedItemPosition == 1) {
                            scoreArray[rowsArray.size - 1][i].text = (rowsArray.size * 10).toString()
                            bonusArray[rowsArray.size-1][i].visibility = Spinner.GONE
                            bonusArray[rowsArray.size-1][i].setSelection(0)
                        } else if (bidsArray[rowsArray.size-1][i].selectedItemPosition == 1 && tricksArray[rowsArray.size-1][i].selectedItemPosition > 1) {
                            scoreArray[rowsArray.size - 1][i].text = (rowsArray.size * -10).toString()
                            bonusArray[rowsArray.size-1][i].visibility = Spinner.GONE
                            bonusArray[rowsArray.size-1][i].setSelection(0)
                        } else if (bidsArray[rowsArray.size-1][i].selectedItemPosition == tricksArray[rowsArray.size-1][i].selectedItemPosition) {
                            when {
                                bonusArray[rowsArray.size - 1][i].selectedItemPosition == 1 -> {
                                    scoreArray[rowsArray.size - 1][i].text = ((
                                            tricksArray[rowsArray.size - 1][i].selectedItemPosition - 1) * 20 + 50
                                            ).toString()
                                }
                                bonusArray[rowsArray.size-1][i].selectedItemPosition > 1 -> {
                                    scoreArray[rowsArray.size - 1][i].text = ((
                                            tricksArray[rowsArray.size - 1][i].selectedItemPosition - 1) * 20 + (bonusArray[rowsArray.size - 1][i].selectedItemPosition - 1) * 30
                                            ).toString()
                                }
                                else -> {
                                    scoreArray[rowsArray.size - 1][i].text =
                                        ((tricksArray[rowsArray.size - 1][i].selectedItemPosition - 1) * 20).toString()
                                }
                            }
                            bonusArray[rowsArray.size-1][i].visibility = Spinner.VISIBLE
                        } else {
                            scoreArray[rowsArray.size-1][i].text = (abs(bidsArray[rowsArray.size-1][i].selectedItemPosition - tricksArray[rowsArray.size-1][i].selectedItemPosition) * (-10)).toString()
                            bonusArray[rowsArray.size-1][i].visibility = Spinner.GONE
                            bonusArray[rowsArray.size-1][i].setSelection(0)
                        }
                    } else {
                        scoreArray[rowsArray.size-1][i].text = ""
                    }

                }
            }
            wizard -> {
                for (i in playersArray.indices) {
                    if ( bidsArray[rowsArray.size-1][i].selectedItemPosition > 0 && tricksArray[rowsArray.size-1][i].selectedItemPosition > 0 ) {
                        if (bidsArray[rowsArray.size-1][i].selectedItemPosition == tricksArray[rowsArray.size-1][i].selectedItemPosition) {
                            scoreArray[rowsArray.size-1][i].text = (20 + (tricksArray[rowsArray.size-1][i].selectedItemPosition-1) * 10).toString()
                        } else {
                            scoreArray[rowsArray.size-1][i].text = (abs(bidsArray[rowsArray.size-1][i].selectedItemPosition - tricksArray[rowsArray.size-1][i].selectedItemPosition) * (-10)).toString()
                        }
                    } else {
                        scoreArray[rowsArray.size-1][i].text = ""
                    }

                }
            }
        }
    }

    private fun checkLogic(): Boolean {
        when (game) {
            rummy -> {
                var countEmpty = 0
                for (i in playersArray.indices) {
                    if ( inputArray[inputArray.size-1][i].text.isEmpty() ) {
                        inputArray[inputArray.size-1][i].setText("0")
                        countEmpty++
                    } else if ( inputArray[inputArray.size-1][i].text.toString() == "0" ) {
                        countEmpty++
                    }
                }
                return when (countEmpty) {
                    1 -> {
                        true
                    }
                    0 -> {
                        showSnackbar(R.string.snackbar_too_many_inputs, Snackbar.LENGTH_SHORT,"")
                        false
                    }
                    else -> {
                        showSnackbar(R.string.snackbar_missing_inputs, Snackbar.LENGTH_SHORT,"")
                        false
                    }
                }
            }
            skullKing -> {
                var emptyBids = false
                var countTricksSum = 0
                for (i in playersArray.indices) {
                    if ( bidsArray[rowsArray.size-1][i].selectedItemPosition == 0 ) emptyBids = true
                }
                if (emptyBids) {
                    showSnackbar(R.string.snackbar_empty_bids, Snackbar.LENGTH_SHORT,"")
                    return false
                } else {
                    for (i in playersArray.indices) {
                        if (tricksArray[rowsArray.size - 1][i].selectedItemPosition == 0) {
                            tricksArray[rowsArray.size - 1][i].setSelection(1)
                        }
                        countTricksSum += tricksArray[rowsArray.size-1][i].selectedItemPosition-1
                    }
                }
                calcScore()
                if ( countTricksSum != rowsArray.size ) {
                    showSnackbar(R.string.snackbar_incorrect_tricks, Snackbar.LENGTH_SHORT,"")
                    return false
                }
                var countMermaidCaughtSkull = 0
                var countSkullCaughtPirates = 0
                var selection: Int
                for (i in playersArray.indices) {
                    selection = bonusArray[rowsArray.size-1][i].selectedItemPosition
                    when {
                        selection == 1 -> countMermaidCaughtSkull++
                        selection > 1 -> countSkullCaughtPirates++
                    }
                }
                when {
                    countMermaidCaughtSkull > 1 -> {
                        showSnackbar(R.string.snackbar_too_many_skulls_cought, Snackbar.LENGTH_SHORT,"")
                        return false
                    }
                    countSkullCaughtPirates > 1 -> {
                        showSnackbar(R.string.snackbar_too_many_pirates_cought, Snackbar.LENGTH_SHORT,"")
                        return false
                    }
                    countMermaidCaughtSkull+countSkullCaughtPirates > 1 -> {
                        showSnackbar(R.string.snackbar_too_many_cought, Snackbar.LENGTH_SHORT,"")
                        return false
                    }
                }
            }
            wizard -> {
                var countTricksSum = 0
                val countBidSum = checkBids()

                if (countBidSum==-1) {
                    showSnackbar(R.string.snackbar_empty_bids, Snackbar.LENGTH_SHORT,"")
                    return false
                } else if (countBidSum == rowsArray.size && !PreferenceManager.getDefaultSharedPreferences(this).getBoolean("pref_wizard_even_bids", true)) {
                    showSnackbar(R.string.snackbar_even_bids_not_allowed, Snackbar.LENGTH_LONG,"")
                    return false
                } else {
                    for (i in playersArray.indices) {
                        if (tricksArray[rowsArray.size - 1][i].selectedItemPosition == 0) {
                            tricksArray[rowsArray.size - 1][i].setSelection(1)
                        }
                        countTricksSum += tricksArray[rowsArray.size-1][i].selectedItemPosition-1
                    }
                }
                calcScore()
                if ( countTricksSum != rowsArray.size ) {
                    showSnackbar(R.string.snackbar_incorrect_tricks, Snackbar.LENGTH_SHORT,"")
                    return false
                }
            }
            addition, addition2 -> {
                for (i in playersArray.indices) {
                    if ( inputArray[inputArray.size-1][i].text.isEmpty() ) {
                        inputArray[inputArray.size-1][i].setText("0")
                    }
                }
                return true
            }
        }
        return true
    }

    private fun checkBids(): Int {
        var countBidSum = 0
        for (i in playersArray.indices) {
            if ( bidsArray[rowsArray.size-1][i].selectedItemPosition == 0 ) {
                return -1
            } else {
                countBidSum += bidsArray[rowsArray.size-1][i].selectedItemPosition-1
            }
        }
        return countBidSum
    }

    private fun displayScores() {
        if (game_sheet.childCount > 0) game_sheet.removeAllViews()
        for (i in rowsArray.indices) {
            game_sheet.addView(rowsArray[i])
        }
        if (game != sixtySix) {
            for (i in playersArray.indices) {
                if (i == (rowsArray.size - 1) % playersArray.size) {
                    playersArray[i].background = getDrawable(R.drawable.player_input_highlighted)
                } else {
                    playersArray[i].background = getDrawable(R.drawable.player_input)
                }
            }
        }
        when (game) {
            rummy -> {
                when (rowsArray.size) {
                    0, 1 -> updateToolbar(getString(R.string.action_game_rummy), getString(R.string.round) + " " + rowsArray.size.toString(),2,2,0,0,0,0,1,2,0, 0)
                    else -> updateToolbar(getString(R.string.action_game_rummy), getString(R.string.round) + " " + rowsArray.size.toString(),2,2,0,0,0,2,2,2,0, 0)
                }
                for (i in rowsArray.indices) {
                    for (j in playersArray.indices) {
                        if (i < rowsArray.size-1) {
                            if (inputArray[i][j].tag == 2) {
                                inputArray[i][j].background = getDrawable(R.drawable.hand_rummy)
                            } else {
                                inputArray[i][j].background = getDrawable(R.drawable.score_display)
                            }
                            inputArray[i][j].isFocusable = false
                        } else {
                            inputArray[i][j].background = getDrawable(R.drawable.score_input_normal)
                            inputArray[i][j].isFocusable = true
                            inputArray[i][j].isFocusableInTouchMode = true
                            inputArray[i][0].requestFocus()
                        }
                    }
                }
            }
            sixtySix -> {
                var scores: List<String>
                var sum: Int
                for (i in playersArray.indices) {
                    player_places.visibility = View.VISIBLE
                    if (placesArray[i].text.isNotEmpty()) {
                        scores = placesArray[i].text.split(",")
                        updateToolbar(
                            getString(R.string.action_game_66), getString(R.string.round) + " " + (scores.size+1).toString(),
                            0,0,0,0,0,2,2,2,0, 0
                        )
                        if (scores.size % 2 == 1) {
                            playersArray[1].background = getDrawable(R.drawable.player_input_highlighted)
                            playersArray[0].background = getDrawable(R.drawable.player_input)
                        } else {
                            playersArray[1].background = getDrawable(R.drawable.player_input)
                            playersArray[0].background = getDrawable(R.drawable.player_input_highlighted)
                        }
                        sum = 0
                        for (j in scores.indices) {
                            sum += scores[j].trim().toInt()
                        }
                        placesArray[i].tag = sum
                        scoreArray[0][i].text = ""
                        for (j in 1..(sum / 10)) {
                            scoreArray[0][i].text = scoreArray[0][i].text.toString() + "0 "
                        }
                        if (sum % 10 > 0) scoreArray[0][i].text =
                            scoreArray[0][i].text.toString() + (sum % 10).toString()
                    } else {
                        updateToolbar(getString(R.string.action_game_66), getString(R.string.round) + " 1",0,0,0,0,0,0,1,2,0, 0)
                        scoreArray[0][i].text=""
                        playersArray[0].background = getDrawable(R.drawable.player_input_highlighted)
                        placesArray[i].hint = getString(R.string.details_rounds)
                    }
                }
            }
            skullKing -> {
                val maxRounds = 10
                when (rowsArray.size) {
                    0, 1 -> updateToolbar(
                        getString(R.string.action_game_skull), getString(
                            R.string.round
                        ) + " " + rowsArray.size.toString() + " " + getString(R.string.of) + " " +maxRounds.toString(), 2, 0, 0, 0, 0, 0,1,2,2, 0
                    )
                    maxRounds ->  updateToolbar(
                        getString(R.string.action_game_skull), getString(
                            R.string.round
                        ) + " " + rowsArray.size.toString() + " " + getString(R.string.of) + " " +maxRounds.toString(), 1, 0, 0, 0, 0, 2,2,2,2, 0
                    )
                    else -> updateToolbar(
                        getString(R.string.action_game_skull), getString(
                            R.string.round
                        ) + " " + rowsArray.size.toString() + " " + getString(R.string.of) + " " +maxRounds.toString(), 2, 0, 0, 0, 0, 2,1,2,2, 0
                    )
                }
                var emptyBids = false
                for (i in playersArray.indices) {
                    if (bidsArray[rowsArray.size-1][i].selectedItemPosition == 0) emptyBids = true
                }
                for (i in rowsArray.indices) {
                    for (j in playersArray.indices) {
                        if (i < rowsArray.size-1) {
                            bidsArray[i][j].visibility = Spinner.GONE
                            tricksArray[i][j].visibility =Spinner.GONE
                            scoreArray[i][j].visibility = TextView.VISIBLE
                            scoreArray[i][j].background = getDrawable(R.drawable.score_display)
                            bonusArray[i][j].visibility =Spinner.GONE
                        } else {
                            bidsArray[i][j].visibility = Spinner.VISIBLE
                            scoreArray[i][j].background = getDrawable(R.drawable.score_input_normal_wizard)
                            if (emptyBids) {
                                tricksArray[i][j].visibility = Spinner.GONE
                                scoreArray[i][j].visibility = TextView.GONE
                                bonusArray[i][j].visibility = Spinner.GONE
                            } else {
                                tricksArray[i][j].visibility = Spinner.VISIBLE
                                scoreArray[i][j].visibility = TextView.VISIBLE
                                if (bidsArray[i][j].selectedItemPosition == tricksArray[i][j].selectedItemPosition && bidsArray[i][j].selectedItemPosition > 1) {
                                    bonusArray[i][j].visibility = Spinner.VISIBLE
                                } else {
                                    bonusArray[i][j].setSelection(0)
                                    bonusArray[i][j].visibility = Spinner.GONE
                                }
                            }
                        }
                    }
                }
            }
            wizard -> {
                val maxRounds = if (playersArray.size == 2) 20 else 60 / playersArray.size
                when (rowsArray.size) {
                    0, 1 -> updateToolbar(
                        getString(R.string.action_game_wizard), getString(
                            R.string.round
                        ) + " " + rowsArray.size.toString() + " " + getString(R.string.of) + " " +maxRounds.toString(), 2, 0, 0, 0, 0, 0,1,2,0, 0
                    )
                    maxRounds ->  updateToolbar(
                        getString(R.string.action_game_wizard), getString(
                            R.string.round
                        ) + " " + rowsArray.size.toString() + " " + getString(R.string.of) + " " +maxRounds.toString(), 1, 0, 0, 0, 0, 2,2,2,0, 0
                    )
                    else -> updateToolbar(
                        getString(R.string.action_game_wizard), getString(
                            R.string.round
                        ) + " " + rowsArray.size.toString() + " " + getString(R.string.of) + " " +maxRounds.toString(), 2, 0, 0, 0, 0, 2,1,2,0, 0
                    )
                }
                var emptyBids = false
                for (i in playersArray.indices) {
                    if (bidsArray[rowsArray.size-1][i].selectedItemPosition == 0) emptyBids = true
                }
                for (i in rowsArray.indices) {
                    for (j in playersArray.indices) {
                        if (i < rowsArray.size-1) {
                            bidsArray[i][j].visibility = Spinner.GONE
                            tricksArray[i][j].visibility =Spinner.GONE
                            scoreArray[i][j].visibility = TextView.VISIBLE
                            scoreArray[i][j].background = getDrawable(R.drawable.score_display)
                        } else {
                            bidsArray[i][j].visibility = Spinner.VISIBLE
                            scoreArray[i][j].background = getDrawable(R.drawable.score_input_normal_wizard)
                            if (emptyBids) {
                                tricksArray[i][j].visibility = Spinner.GONE
                                scoreArray[i][j].visibility = TextView.GONE
                            } else {
                                tricksArray[i][j].visibility = Spinner.VISIBLE
                                scoreArray[i][j].visibility = TextView.VISIBLE
                            }
                        }
                    }
                }
            }
            addition -> {
                when (rowsArray.size) {
                    0, 1 -> updateToolbar(getString(R.string.action_game_addition), getString(R.string.round) + " " + rowsArray.size.toString(),2,0,0,0,0,0,1,2,0, 0)
                    else -> updateToolbar(getString(R.string.action_game_addition), getString(R.string.round) + " " + rowsArray.size.toString(),2,0,0,0,0,2,2,2,0, 0)
                }
                for (i in rowsArray.indices) {
                    for (j in playersArray.indices) {
                        if (i < rowsArray.size-1) {
                            inputArray[i][j].background = getDrawable(R.drawable.score_display)
                            inputArray[i][j].isFocusable = false
                        } else {
                            inputArray[i][j].background = getDrawable(R.drawable.score_input_normal)
                            inputArray[i][j].isFocusable = true
                            inputArray[i][j].isFocusableInTouchMode = true
                            inputArray[i][0].requestFocus()
                        }
                    }
                }
            }
            addition2 -> {
                when (rowsArray.size) {
                    0, 1 -> updateToolbar(getString(R.string.action_game_addition2), getString(R.string.round) + " " + rowsArray.size.toString(),2,0,0,0,0,0,1,2,0, 0)
                    else -> updateToolbar(getString(R.string.action_game_addition2), getString(R.string.round) + " " + rowsArray.size.toString(),2,0,0,0,0,2,2,2,0, 0)
                }
                for (i in rowsArray.indices) {
                    for (j in playersArray.indices) {
                        if (i < rowsArray.size-1) {
                            inputArray[i][j].background = getDrawable(R.drawable.score_display)
                            inputArray[i][j].isFocusable = false
                        } else {
                            inputArray[i][j].background = getDrawable(R.drawable.score_input_normal)
                            inputArray[i][j].isFocusable = true
                            inputArray[i][j].isFocusableInTouchMode = true
                            inputArray[i][0].requestFocus()
                        }
                    }
                }
            }
        }
        val imm: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(player_names.windowToken, 0)
        setTextSizes()
        scroll_view.post {
            scroll_view.fullScroll(View.FOCUS_DOWN)
        }
    }

    private fun calcTotals() {
        var sum: Int
        for (i in playersArray.indices) {
            sum = 0
                when (game) {
                    rummy, addition, addition2 -> {
                        for (j in inputArray.indices) {
                            if ( inputArray[j][i].text.isNotEmpty() ) sum += inputArray[j][i].text.toString().toInt()
                        }
                    }
                    skullKing -> {
                        for (j in scoreArray.indices) {
                            if (scoreArray[j][i].text.isNotEmpty()) {
                                sum += scoreArray[j][i].text.toString().toInt()
                            }
                        }
                    }
                    wizard -> {
                        for (j in scoreArray.indices) {
                            if (scoreArray[j][i].text.isNotEmpty()) sum += scoreArray[j][i].text.toString()
                                .toInt()
                        }
                    }
                }
                totalsArray[i].text = sum.toString()
        }
    }
    
    private fun updateToolbar(title: String, subtitle: String, addRow: Int, addRowDouble: Int, addPlayer: Int, removePlayer: Int, startGame: Int, removeRow: Int, endGame: Int, resetGame: Int, playSound: Int, newGame: Int) {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        toolbar.title = title
        toolbar.subtitle = subtitle
        toolbar.menu.findItem(R.id.action_add_row_normal).isVisible = addRow > 0
        toolbar.menu.findItem(R.id.action_add_row_double).isVisible = addRowDouble > 0
        toolbar.menu.findItem(R.id.action_add_player).isVisible = addPlayer > 0
        toolbar.menu.findItem(R.id.action_remove_player).isVisible = removePlayer > 0
        toolbar.menu.findItem(R.id.action_start_game).isVisible = startGame > 0
        toolbar.menu.findItem(R.id.action_remove_round).isVisible = removeRow > 0
        toolbar.menu.findItem(R.id.action_end_the_game).isVisible = endGame > 0
        toolbar.menu.findItem(R.id.action_reset_game).isVisible = resetGame > 0
        toolbar.menu.findItem(R.id.action_play_sound).isVisible = playSound > 0
        toolbar.menu.findItem(R.id.action_new_game).isVisible = newGame > 0

        toolbar.menu.findItem(R.id.action_add_row_normal).isEnabled = addRow == 2
        toolbar.menu.findItem(R.id.action_add_row_double).isEnabled = addRowDouble == 2
        toolbar.menu.findItem(R.id.action_add_player).isEnabled = addPlayer == 2
        toolbar.menu.findItem(R.id.action_remove_player).isEnabled = removePlayer == 2
        toolbar.menu.findItem(R.id.action_start_game).isEnabled = startGame == 2
        toolbar.menu.findItem(R.id.action_remove_round).isEnabled = removeRow == 2
        toolbar.menu.findItem(R.id.action_end_the_game).isEnabled = endGame == 2
        toolbar.menu.findItem(R.id.action_reset_game).isEnabled = resetGame == 2
        toolbar.menu.findItem(R.id.action_play_sound).isEnabled = playSound == 2
        toolbar.menu.findItem(R.id.action_new_game).isEnabled = newGame == 2
    }

    private fun initializeGame() {
        content_main.keepScreenOn=PreferenceManager.getDefaultSharedPreferences(this).getBoolean("pref_keep_screen_on", true)
        game_over = false
        if (game_sheet.childCount > 0) game_sheet.removeAllViews()
        when (game) {
            rummy, sixtySix, skullKing, wizard, addition, addition2 -> {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR)
                inputArray = arrayOf()
                rowsArray = arrayOf()
                bidsArray = arrayOf()
                tricksArray = arrayOf()
                scoreArray = arrayOf()
                bonusArray = arrayOf()
                for (i in playersArray.indices) {
                    playersArray[i].isFocusable = true
                    playersArray[i].isFocusableInTouchMode = true
                    playersArray[i].background = getDrawable(R.drawable.player_input)
                    namesArray[i].setSelection(0)
                    placesArray[i].text = ""
                    totalsArray[i].text = ""
                }
                if (playersArray.isEmpty()) {
                    addPlayer()
                    addPlayer()
                    playersArray[0].requestFocus()
                } else displayPlayer()
                player_names_spinner.visibility = View.VISIBLE
                player_places.visibility = TextView.GONE
                totals.visibility = View.GONE
                footer.visibility = View.VISIBLE
            }
            yahtzee -> {
//                Toast.makeText(this, player_names.childCount.toString(), Toast.LENGTH_SHORT).show()
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                if (player_names.childCount > 0) player_names.removeAllViews()
                if (player_names_spinner.childCount > 0) player_names_spinner.removeAllViews()
                if (player_places.childCount > 0) player_places.removeAllViews()
                if (totals.childCount > 0) totals.removeAllViews()
                game_sheet.removeAllViews()
                updateToolbar(getString(R.string.action_game_yahtzee),"",0,0,0,0,0,0,0,0,0, 2)
                var yahtzeeView: View = layoutInflater.inflate(R.layout.content_yahtzee, null)
                game_sheet.addView(yahtzeeView)
                val yahtzeeLayout = findViewById<LinearLayout>(R.id.content_yahtzee) as LinearLayout
                val params: LinearLayout.LayoutParams = yahtzeeLayout.layoutParams as LinearLayout.LayoutParams
                params.height = scroll_view.height
                yahtzeeLayout.layoutParams = params
                var viewYahtzee: EditText
                for (i in 1..6) {
                    for (j in 1..6) {
                        viewYahtzee=findViewById(resources.getIdentifier("yahtzee_"+j.toString() + "_"+i.toString(), "id", packageName))
                        viewYahtzee.addTextChangedListener(textWatcherYahtzee(viewYahtzee,j))
                    }
                    viewYahtzee=findViewById(resources.getIdentifier("yahtzee_3k_" + i.toString(), "id", packageName))
                    viewYahtzee.addTextChangedListener(textWatcherYahtzee(viewYahtzee,6))
                    viewYahtzee=findViewById(resources.getIdentifier("yahtzee_4k_" + i.toString(), "id", packageName))
                    viewYahtzee.addTextChangedListener(textWatcherYahtzee(viewYahtzee,6))
                    viewYahtzee=findViewById(resources.getIdentifier("yahtzee_chance_" + i.toString(), "id", packageName))
                    viewYahtzee.addTextChangedListener(textWatcherYahtzee(viewYahtzee,6))
                }

            }
            else -> {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                if (player_names.childCount > 0) player_names.removeAllViews()
                if (player_places.childCount > 0) player_places.removeAllViews()
                if (totals.childCount > 0) totals.removeAllViews()
                updateToolbar(getString(R.string.app_name),"",0,0,0,0,0,0,0,0,0, 0)
                val welcomeView: View = layoutInflater.inflate(R.layout.content_welcome, null)
                game_sheet.addView(welcomeView)
                player_names_spinner.visibility = View.GONE
                footer.visibility = View.VISIBLE
                footer.text = getString(R.string.footer,BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE)
            }
        }
    }

    private fun textWatcherYahtzee(yahtzeeEditText: EditText, yahtzeeInput: Int): TextWatcher? {
        val textWatcher: TextWatcher? = object : TextWatcher {
            var ignoreChange=false
            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                if (!ignoreChange && yahtzeeEditText.isFocusable){
                    ignoreChange=true
                    yahtzeeEditText.setText(s.replaceFirst("^0+(?!$)".toRegex(), ""))
                    yahtzeeEditText.setSelection(yahtzeeEditText.text.length)
                    ignoreChange=false
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub
            }

            override fun afterTextChanged(s: Editable) {
                if (s.toString()== "0") {
                    yahtzeeEditText.setText("X")
                } else if (s.length>1) {
//                    Toast.makeText(applicationContext, s.subSequence(0,1).toString(), Toast.LENGTH_SHORT).show()
                    if (s.subSequence(0, 1).toString() == "X") {
                        yahtzeeEditText.setText(s.subSequence(1,2).toString())
                    }
                    if (s.subSequence(1, 2).toString() == "X") {
                        yahtzeeEditText.setText(s.subSequence(0,1).toString())
                    }
                }
                if (yahtzeeEditText.text.length>0 && yahtzeeEditText.text.toString() != "X") {
                    val value=yahtzeeEditText.text.toString().toInt()
                    when (yahtzeeInput) {
                        1 -> {
                            if (value > 5) {
                                yahtzeeEditText.setText("5")
                                showSnackbar(R.string.snackbar_yahtzee_limit, Snackbar.LENGTH_LONG,"5")
                            }
                        }
                        2 -> {
                            if (value > 10) {
                                yahtzeeEditText.setText("10")
                                showSnackbar(R.string.snackbar_yahtzee_limit, Snackbar.LENGTH_LONG,"10")
                            }
                        }
                        3 -> {
                            if (value > 15) {
                                yahtzeeEditText.setText("15")
                                showSnackbar(R.string.snackbar_yahtzee_limit, Snackbar.LENGTH_LONG,"15")
                            }
                        }
                        4 -> {
                            if (value > 20) {
                                yahtzeeEditText.setText("20")
                                showSnackbar(R.string.snackbar_yahtzee_limit, Snackbar.LENGTH_LONG,"20")
                            }
                        }
                        5 -> {
                            if (value > 25) {
                                yahtzeeEditText.setText("25")
                                showSnackbar(R.string.snackbar_yahtzee_limit, Snackbar.LENGTH_LONG,"25")
                            }
                        }
                        6 -> {
                            if (value > 30) {
                                yahtzeeEditText.setText("30")
                                showSnackbar(R.string.snackbar_yahtzee_limit, Snackbar.LENGTH_LONG,"30")
                            }
                        }
                    }
                }
                calcYahtzee(yahtzeeEditText.id)
            }
        }
        return textWatcher
    }

    private fun calcYahtzee(id: Int): Boolean {
        var total1=IntArray(6)
        var total2=IntArray(6)
        var total=IntArray(6)
        var score: String
        var totaltotal=true
        var noValues=true
        for (j in 1..6) {
            var total1_valid=true
            var total2_valid=true
            for (i in 1..6) {
                var viewYahtzee = findViewById<EditText>(resources.getIdentifier("yahtzee_" + i.toString() + "_" + j.toString(),"id",packageName)
                )
                score = viewYahtzee.text.toString()
                if (score.length > 0 && score != "X") {
                    total1[j-1] += score.toInt()
                    if (score.toInt() % i > 0) {
                        viewYahtzee.setBackgroundColor(ContextCompat.getColor(this, R.color.colorYahtzeeInvalid))
                        total1_valid=false
                    } else {
                        yahtzeeSetLocked(viewYahtzee, id)
                    }
                    noValues=false
                } else if (score == "X") {
                    yahtzeeSetLocked(viewYahtzee, id)
                    noValues=false
                } else {
                    viewYahtzee.setBackgroundColor(ContextCompat.getColor(this, R.color.colorYahtzeeInput))
                }
            }
            var viewYahtzeeTotal1 = findViewById<TextView>(
                resources.getIdentifier(
                    "total_1_" + j.toString(),
                    "id",
                    packageName
                )
            )
            if (total1_valid) {
                if (total1[j - 1] > 62) {
                    total1[j - 1] += 35
                }
                if (total1[j - 1] > 0) {
                    viewYahtzeeTotal1.setText(total1[j - 1].toString())
                } else {
                    viewYahtzeeTotal1.setText("")
                }
            } else {
                viewYahtzeeTotal1.setText("???")
            }
// total2
            var viewYahtzee3k = findViewById<EditText>(resources.getIdentifier("yahtzee_3k_" + j.toString(),"id",packageName))
            score = viewYahtzee3k.text.toString()
            if (score.length > 0 && score != "X") {
                total2[j-1] += score.toInt()
                if (score.toInt() < 5) {
                    viewYahtzee3k.setBackgroundColor(ContextCompat.getColor(this, R.color.colorYahtzeeInvalid))
                    total2_valid=false
                } else {
                    yahtzeeSetLocked(viewYahtzee3k, id)
                }
                noValues=false
            } else if (score == "X") {
                yahtzeeSetLocked(viewYahtzee3k, id)
                noValues=false
            } else {
                viewYahtzee3k.setBackgroundColor(ContextCompat.getColor(this, R.color.colorYahtzeeInput))
            }
            var viewYahtzee4k = findViewById<EditText>(resources.getIdentifier("yahtzee_4k_" + j.toString(),"id",packageName))
            score = viewYahtzee4k.text.toString()
            if (score.length > 0 && score != "X") {
                total2[j-1] += score.toInt()
                if (score.toInt() < 5) {
                    viewYahtzee4k.setBackgroundColor(ContextCompat.getColor(this, R.color.colorYahtzeeInvalid))
                    total2_valid=false
                } else {
                    yahtzeeSetLocked(viewYahtzee4k, id)
                }
                noValues=false
            } else if (score == "X") {
                yahtzeeSetLocked(viewYahtzee4k, id)
                noValues=false
            } else {
                viewYahtzee4k.setBackgroundColor(ContextCompat.getColor(this, R.color.colorYahtzeeInput))
            }
            var viewYahtzeeFH = findViewById<TextView>(resources.getIdentifier("yahtzee_full_house_" + j.toString(),"id",packageName))
            score = viewYahtzeeFH.text.toString()
            if (score.length > 0 && score != "X") {
                total2[j-1] += score.toInt()
                noValues=false
                yahtzeeSetLocked(viewYahtzeeFH, id)
            } else if (score == "X") {
                noValues=false
                yahtzeeSetLocked(viewYahtzeeFH, id)
            }
            var viewYahtzeeSS = findViewById<TextView>(resources.getIdentifier("yahtzee_small_straight_" + j.toString(),"id",packageName))
            score = viewYahtzeeSS.text.toString()
            if (score.length > 0 && score != "X") {
                total2[j-1] += score.toInt()
                noValues=false
                yahtzeeSetLocked(viewYahtzeeSS, id)
            } else if (score == "X") {
                noValues=false
                yahtzeeSetLocked(viewYahtzeeSS, id)
            }
            var viewYahtzeeLS = findViewById<TextView>(resources.getIdentifier("yahtzee_large_straight_" + j.toString(),"id",packageName))
            score = viewYahtzeeLS.text.toString()
            if (score.length > 0 && score != "X") {
                total2[j-1] += score.toInt()
                noValues=false
                yahtzeeSetLocked(viewYahtzeeLS, id)
            } else if (score == "X") {
                noValues=false
                yahtzeeSetLocked(viewYahtzeeLS, id)
            }
            var viewYahtzeeY = findViewById<TextView>(resources.getIdentifier("yahtzee_yahtzee_" + j.toString(),"id",packageName))
            score = viewYahtzeeY.text.toString()
            if (score.length > 0 && score != "X") {
                total2[j-1] += score.toInt()
                noValues=false
                yahtzeeSetLocked(viewYahtzeeY, id)
            } else if (score == "X") {
                noValues=false
                yahtzeeSetLocked(viewYahtzeeY, id)
            }
            var viewYahtzeeC = findViewById<EditText>(resources.getIdentifier("yahtzee_chance_" + j.toString(),"id",packageName))
            score = viewYahtzeeC.text.toString()
            if (score.length > 0 && score != "X") {
                total2[j-1] += score.toInt()
                if (score.toInt() < 5) {
                    viewYahtzeeC.setBackgroundColor(ContextCompat.getColor(this, R.color.colorYahtzeeInvalid))
                    total2_valid=false
                } else {
                    yahtzeeSetLocked(viewYahtzeeC, id)
                }
                noValues=false
            } else if (score == "X") {
                noValues=false
                yahtzeeSetLocked(viewYahtzeeC, id)
            } else {
                viewYahtzeeC.setBackgroundColor(ContextCompat.getColor(this, R.color.colorYahtzeeInput))
            }
            var viewYahtzeeTotal2 = findViewById<TextView>(
                resources.getIdentifier(
                    "total_2_" + j.toString(),
                    "id",
                    packageName
                )
            )
            if (total2_valid) {
                if (total2[j - 1] > 0) {
                    viewYahtzeeTotal2.setText(total2[j - 1].toString())
                } else {
                    viewYahtzeeTotal2.setText("")
                }
            } else {
                viewYahtzeeTotal2.setText("???")
            }
            var viewYahtzeeTotal = findViewById<TextView>(
                resources.getIdentifier(
                    "total_" + j.toString(),
                    "id",
                    packageName
                )
            )
            if (total1_valid && total2_valid) {
                if (total1[j - 1] > 0 || total2[j - 1] > 0) {
                    total[j-1]=total1[j-1]+total2[j-1]
                    viewYahtzeeTotal.setText(total[j - 1].toString())
                } else {
                    viewYahtzeeTotal.setText("")
                }
            } else {
                totaltotal=false
                viewYahtzeeTotal.setText("???")
            }
        }
        if (totaltotal && total.sum()>0) {
            updateToolbar(getString(R.string.action_game_yahtzee),getString(R.string.yahtzee_total_total)+" "+total.sum().toString(),0,0,0,0,0,0,0,0,0, 2)
        } else {
            updateToolbar(getString(R.string.action_game_yahtzee),getString(R.string.yahtzee_total_total),0,0,0,0,0,0,0,0,0, 2)
        }
        return noValues
    }

    private fun yahtzeeSetLocked(view: View, id: Int) {
        if (view.id != id) {
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorYahtzeeLocked))
            view.isFocusable = false
            view.isClickable = false
        } else {
            view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorYahtzeeInput))
        }
    }

    private fun addPlayer() {
        val latestPlayerIndex = playersArray.size
        val marginHorizontal = dp2px(2f)
        val marginVertical = dp2px(8f)
        val paddingHorizontal = dp2px(5f)
        val paddingVertical = dp2px(7f)
        val playersParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f)
        playersParams.setMargins(marginHorizontal,marginVertical,marginHorizontal,marginVertical)
        val namesParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT,1f)
        namesParams.setMargins(marginHorizontal,0,marginHorizontal,marginVertical)
        val placesParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f)
        placesParams.setMargins(marginHorizontal+2,0,marginHorizontal+2,marginVertical)
        val totalsParams: LinearLayout.LayoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f)
        totalsParams.setMargins(marginHorizontal,0,marginHorizontal,0)
        playersArray += EditText(player_names.context)
        playersArray[latestPlayerIndex].layoutParams = playersParams
        playersArray[latestPlayerIndex].background = ContextCompat.getDrawable(this, R.drawable.player_input)
        playersArray[latestPlayerIndex].textSize = textSizePlayerNames
        playersArray[latestPlayerIndex].textAlignment = TextView.TEXT_ALIGNMENT_CENTER
        playersArray[latestPlayerIndex].typeface = Typeface.DEFAULT_BOLD
        playersArray[latestPlayerIndex].setPadding(paddingHorizontal,paddingVertical,paddingHorizontal,paddingVertical)
        playersArray[latestPlayerIndex].hint = resources.getString(R.string.name) + " " + (latestPlayerIndex+1).toString()
        playersArray[latestPlayerIndex].setSelectAllOnFocus(true)
        if (latestPlayerIndex>0) playersArray[latestPlayerIndex-1].imeOptions = EditorInfo.IME_ACTION_NEXT
        playersArray[latestPlayerIndex].inputType = EditorInfo.TYPE_TEXT_FLAG_CAP_WORDS
        playersArray[latestPlayerIndex].setSingleLine()

        namesArray += Spinner(player_names.context)
        namesArray[latestPlayerIndex].layoutParams = namesParams
        namesArray[latestPlayerIndex].background = getDrawable(R.drawable.input_favorite_names)
//        namesArray[latestPlayerIndex].textSize = textSizePlayerNames
        namesArray[latestPlayerIndex].onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                val selectedItem =
                    parent.getItemAtPosition(position).toString()
                if (position>0) {
                    playersArray[latestPlayerIndex].setText(selectedItem)
                    namesArray[latestPlayerIndex].setSelection(0)
                }
            }
            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }

        placesArray += TextView(player_places.context)
        placesArray[latestPlayerIndex].layoutParams = placesParams
        placesArray[latestPlayerIndex].textSize = 15f
        placesArray[latestPlayerIndex].setTextColor(ContextCompat.getColor(this,R.color.colorPrimaryDark))
        placesArray[latestPlayerIndex].textAlignment = TextView.TEXT_ALIGNMENT_CENTER
        placesArray[latestPlayerIndex].typeface = Typeface.DEFAULT_BOLD
        placesArray[latestPlayerIndex].setPadding(paddingHorizontal,paddingVertical,paddingHorizontal,paddingVertical)
        placesArray[latestPlayerIndex].background = getDrawable(R.drawable.places)

        totalsArray += TextView(totals.context)
//        totals.setPadding(dp2px(3f),dp2px(8f),dp2px(3f),dp2px(8f))
        totalsArray[latestPlayerIndex].layoutParams = totalsParams
        totalsArray[latestPlayerIndex].background = ContextCompat.getDrawable(this, R.drawable.totals)
        totalsArray[latestPlayerIndex].setTextColor(ContextCompat.getColor(this,R.color.colorTotalsText))
        totalsArray[latestPlayerIndex].textSize = 16f
        totalsArray[latestPlayerIndex].textAlignment = TextView.TEXT_ALIGNMENT_CENTER
        totalsArray[latestPlayerIndex].typeface = Typeface.DEFAULT_BOLD
        totalsArray[latestPlayerIndex].setPadding(paddingHorizontal,paddingVertical/2,paddingHorizontal,paddingVertical/2)

        displayPlayer()

        if (latestPlayerIndex>0) playersArray[latestPlayerIndex].requestFocus()
    }

    private fun removePlayer() {
        playersArray = playersArray.dropLast(1).toTypedArray()
        placesArray = placesArray.dropLast(1).toTypedArray()
        totalsArray = totalsArray.dropLast(1).toTypedArray()
        playersArray[playersArray.size-1].imeOptions = EditorInfo.IME_ACTION_DONE
        displayPlayer()
    }

    private fun displayPlayer() {
        if (player_names.childCount > 0) player_names.removeAllViews()
        if (player_names_spinner.childCount > 0) player_names_spinner.removeAllViews()
        if (player_places.childCount > 0) player_places.removeAllViews()
        if (totals.childCount > 0) totals.removeAllViews()
        when (game) {
            rummy -> {
                when {
                    playersArray.size == 8 -> {
                        updateToolbar(resources.getString(R.string.action_game_rummy),"",0,0,1,2,2,0,0,2,0, 0)
                    }
                    playersArray.size > 2 -> {
                        updateToolbar(resources.getString(R.string.action_game_rummy),"",0,0,2,2,2,0,0,2,0, 0)
                    }
                    else -> {
                        updateToolbar(resources.getString(R.string.action_game_rummy),"",0,0,2,1,2,0,0,2,0, 0)
                    }
                }
            }
            sixtySix -> {
                if ( playersArray.size > 2 ) playersArray = playersArray.dropLast(playersArray.size-2).toTypedArray()
                updateToolbar(resources.getString(R.string.action_game_66),"",0,0,0,0,2,0,0,2,0, 0)
            }
            skullKing -> {
                if ( playersArray.size > 6 ) playersArray = playersArray.dropLast(playersArray.size-6).toTypedArray()
                when {
                    playersArray.size == 6 -> {
                        updateToolbar(resources.getString(R.string.action_game_skull),"",0,0,1,2,2,0,0,2,0, 0)
                    }
                    playersArray.size > 2 -> {
                        updateToolbar(resources.getString(R.string.action_game_skull),"",0,0,2,2,2,0,0,2,0, 0)
                    }
                    else -> {
                        updateToolbar(resources.getString(R.string.action_game_skull),"",0,0,2,1,2,0,0,2,0, 0)
                    }
                }
            }
            wizard -> {
                if ( playersArray.size > 6 ) playersArray = playersArray.dropLast(playersArray.size-6).toTypedArray()
                when {
                    playersArray.size == 6 -> {
                        updateToolbar(resources.getString(R.string.action_game_wizard),"",0,0,1,2,2,0,0,2,0, 0)
                    }
                    playersArray.size > 2 -> {
                        updateToolbar(resources.getString(R.string.action_game_wizard),"",0,0,2,2,2,0,0,2,0, 0)
                    }
                    else -> {
                        updateToolbar(resources.getString(R.string.action_game_wizard),"",0,0,2,1,2,0,0,2,0, 0)
                    }
                }
            }
            addition -> {
                when {
                    playersArray.size == 8 -> {
                        updateToolbar(resources.getString(R.string.action_game_addition),"",0,0,1,2,2,0,0,2,0, 0)
                    }
                    playersArray.size > 2 -> {
                        updateToolbar(resources.getString(R.string.action_game_addition),"",0,0,2,2,2,0,0,2,0, 0)
                    }
                    else -> {
                        updateToolbar(resources.getString(R.string.action_game_addition),"",0,0,2,1,2,0,0,2,0, 0)
                    }
                }
            }
            addition2 -> {
                when {
                    playersArray.size == 8 -> {
                        updateToolbar(resources.getString(R.string.action_game_addition2),"",0,0,1,2,2,0,0,2,0, 0)
                    }
                    playersArray.size > 2 -> {
                        updateToolbar(resources.getString(R.string.action_game_addition2),"",0,0,2,2,2,0,0,2,0, 0)
                    }
                    else -> {
                        updateToolbar(resources.getString(R.string.action_game_addition2),"",0,0,2,1,2,0,0,2,0, 0)
                    }
                }
            }
        }
        for (i in playersArray.indices) {
            when (game) {
                rummy, skullKing, wizard, addition, addition2 -> totals.addView(totalsArray[i])
            }
            player_names.addView(playersArray[i])

            val spinnerAdapter = object : ArrayAdapter<CharSequence?>(
                this,
                R.layout.spinner_names_item, ArrayList<String>() as List<CharSequence?>
            ) {
                override fun isEnabled(position: Int): Boolean {
                    return position != 0
                }

                // Change color item
                override fun getDropDownView(
                    position: Int, convertView: View?,
                    parent: ViewGroup
                ): View? {
                    // TODO Auto-generated method stub
                    val mView = super.getDropDownView(position, convertView, parent)
                    val mTextView = mView as TextView
                    if (position == 0) {
                        mTextView.setTextColor(ContextCompat.getColor(context, R.color.colorHints))
                        mTextView.textSize=20f
                        mTextView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorFavoriteHintBackground))
                    } else {
                        mTextView.setTextColor(ContextCompat.getColor(context, R.color.colorBidText))
                        mTextView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorBidBackground))
                        mTextView.textSize=18f
                    }
                    return mView
                }
            }
            spinnerAdapter.add("\u2605")
            val settings: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
            var playerNames=settings.getString("player_names","").toString().lines()
            if (playerNames[0].isEmpty()) {
                playerNames= getString(R.string.settings_default_player).lines()
            }
            for (i in 0..(playerNames.size-1)) {
                spinnerAdapter.add(playerNames[i])
            }
            spinnerAdapter.setDropDownViewResource(R.layout.spinner_names)
            namesArray[i].adapter = spinnerAdapter
            player_names_spinner.addView(namesArray[i])
            player_places.addView(placesArray[i])
        }
        setTextSizes()
    }

    private fun dp2px(value: Float) :Int {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, resources.displayMetrics).toInt()
    }

    fun showSnackbar( stringId: Int , duration: Int, parameter: String) {
        val snackbar = Snackbar
            .make(findViewById(R.id.toolbar), String.format(resources.getString(stringId),parameter), duration)
            .setAction("OK") { }
        snackbar.view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorSnackbarBackground))
        snackbar.setActionTextColor(ContextCompat.getColor(this, R.color.colorSnackbarText))
        // Changing action button text color
        val sbView = snackbar.view
        val textView = sbView.findViewById<View>(R.id.snackbar_text) as TextView
        textView.setTextColor(ContextCompat.getColor(this, R.color.colorSnackbarText))
        textView.textSize = 16f
        sbView.setBackgroundResource(R.color.colorSnackbarBackground)
        snackbar.show()
    }

    private fun setTextSizes() {
        val screenWidth=resources.configuration.screenWidthDp.toFloat()
        val screenHeight=resources.configuration.screenHeightDp.toFloat()
        val calcBase: Float
        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            calcBase = screenWidth
        } else {
            calcBase = screenWidth
        }
        var playerTextSize = calcBase/15/sqrt(playersArray.size.toFloat())
        if (playerTextSize > 20) playerTextSize=20f
        var placesTextSize = calcBase/18/sqrt(playersArray.size.toFloat())
        if (placesTextSize > 20) placesTextSize=18f
        var totalsTextSize = calcBase/10/sqrt(playersArray.size.toFloat())
        if (totalsTextSize > 20) totalsTextSize=20f
        var inputTextSize = calcBase/12/sqrt(playersArray.size.toFloat())
        if (inputTextSize > 20) inputTextSize=20f
        var scoreTextSize = calcBase/18/sqrt(playersArray.size.toFloat())
        if (scoreTextSize > 18) scoreTextSize=18f
        for (i in playersArray.indices) {
            playersArray[i].textSize = playerTextSize
            totalsArray[i].textSize = totalsTextSize
            when (game) {
                rummy, skullKing, wizard, addition, addition2-> {
                    placesArray[i].textSize = placesTextSize
                }
                sixtySix -> {
                    if (game_over) placesArray[i].textSize = placesTextSize else placesArray[i].textSize = 14f
                }
            }
            for (j in inputArray.indices) {
                inputArray[j][i].textSize =  inputTextSize
            }
            for (j in scoreArray.indices) {
                when (game) {
                    rummy, addition, addition2 -> scoreArray[j][i].textSize = scoreTextSize
                    skullKing, wizard -> scoreArray[j][i].textSize = 17f
                    sixtySix ->  scoreArray[j][i].textSize =  50f
                }
            }
        }
    }
}