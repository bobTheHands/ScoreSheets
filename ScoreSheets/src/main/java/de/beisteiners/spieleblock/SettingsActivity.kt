/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Ronny Steiner
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package de.beisteiners.spieleblock

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment())
            .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
            var playerNamesPref: EditTextPreference? = findPreference("player_names")
            playerNamesPref?.onPreferenceChangeListener = object:
                Preference.OnPreferenceChangeListener {
                override fun onPreferenceChange(preference: Preference?, newValue: Any?): Boolean {
                    val newValueList=newValue.toString().lines()
                    var newString =""
                    for (i in 0..newValueList.size-1) {
                        var stringValue=newValueList[i].trim()
                        if (!stringValue.isEmpty() && newString.length==0) {
                            newString=stringValue
                        } else if (!stringValue.isEmpty() && newString.length>0) {
                            newString=newString+"\n"+stringValue
                        }
                    }
//                    Toast.makeText(getContext(), newString, Toast.LENGTH_SHORT).show()
                    if (newString!=newValue.toString()) {
                        val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
                        var settingsEdit = prefs.edit()
                        settingsEdit.putString("player_names", newString)
                        settingsEdit.apply()
                        activity?.recreate()
                        return false
                    }
                    return true
                }
            }
        }
    }
}